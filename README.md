# Delay

A Eurorack delay module based on the PT2399 digital delay chip.

## Troubleshooting

* Input at pin 16 should be amplified input from initial opamp.

* TODO: opamp amplifies input, but why is there no signal when pin 16 is connected to signal?
  Disconnecting pin 16 from input restores signal on opamp

* Try https://www.youtube.com/watch?v=E2ArVylZb4k&list=PLqSoA_Bh1ZuqudqWaRW4mh7Mri57t7Nm6
* Try https://www.youtube.com/watch?v=nAMRiwxmNzg

~~Hmm. Nothing is working, though I haven't tried the above youtube vids since they don't show schematics.
The datasheet's basic "Echo" circuit doesn't work for me; maybe separate digital and analog ground?
Next try https://www.petervis.com/guitar-circuits/pt2399/basic-echo-circuit.html
And/or https://www.petervis.com/guitar-circuits/pt2399/basic-echo-circuit.html has good explanation and troubleshooting.~~

Aha! It was bad breadboard technique, the signal was being shorted out.


## References

* Skull & Circuits [DLY-1 Lofi-Delay](https://www.skullandcircuits.com/dly-1-lofi-delay/)
* nonlinearcircuits [DelayNoMore](http://www.sdiy.org/pinky/data/DelayNoMore%20build%20notes.pdf)
* Waveform Magazine Project 1 - [PT2399 Delay Module - DIY Build](https://finstudios.com/waveform-magazine-pt2399-delay-diy-build/)
* Here's a good [video](https://www.youtube.com/watch?v=N7Y_1qoq_eM) describing the operation of the "Boy In Well" guitar pedal of a similar design
